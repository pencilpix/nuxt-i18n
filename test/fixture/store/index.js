import Vuex from 'vuex';
import Vue from 'vue';

const createStore = () => {
  return new Vuex.Store({
    state: {},

    mutations: {},

    actions: {},

    getters: {},
  });
};

export default createStore;
