const { resolve } = require('path')

module.exports = {
  rootDir: resolve(__dirname, '../..'),
  srcDir: __dirname,
  dev: false,
  env: {
    dev: true,
  },
  render: {
    resourceHints: false
  },
  modules: [
    [
      '@@',
      {
        hostLocales: true,
        useDefaultAsRoot: true,
        appUrl: 'http://nuxti18n.localhost',
        enableSeo: true,
        // loadFromExternalApi: true,
        // asyncLoadFromCallback: true
        auth: {
          username: 'pencilpix',
          password: 'no1cando',
        }
      },
    ],
  ],

  axios: {
    baseURL: 'http://localhost:4000',
    browserBaseURL: 'http://nuxti18n.localhost',
  }
}
