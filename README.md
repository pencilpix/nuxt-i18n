# @pencilpix/nuxt-vuex-i18n
[![npm (scoped with tag)](https://img.shields.io/npm/v/@pencilpix/nuxt-vuex-i18n/latest.svg?style=flat-square)](https://npmjs.com/package/@pencilpix/nuxt-vuex-i18n)
[![npm](https://img.shields.io/npm/dt/@pencilpix/nuxt-vuex-i18n.svg?style=flat-square)](https://npmjs.com/package/@pencilpix/nuxt-vuex-i18n)
[![CircleCI](https://img.shields.io/circleci/project/github/https://gitlab.com/pencilpix/nuxt-i18n.svg?style=flat-square)](https://circleci.com/gh/https://gitlab.com/pencilpix/nuxt-i18n)
[![Codecov](https://img.shields.io/codecov/c/github/https://gitlab.com/pencilpix/nuxt-i18n.svg?style=flat-square)](https://codecov.io/gh/https://gitlab.com/pencilpix/nuxt-i18n)
[![Dependencies](https://david-dm.org/https://gitlab.com/pencilpix/nuxt-i18n/status.svg?style=flat-square)](https://david-dm.org/https://gitlab.com/pencilpix/nuxt-i18n)
[![js-standard-style](https://img.shields.io/badge/code_style-standard-brightgreen.svg?style=flat-square)](http://standardjs.com)

> nuxt module to handle locales and multiple languages depending on vuex

[📖 **Release Notes**](./CHANGELOG.md)

## Features

The module features

## Setup
- Add `@pencilpix/nuxt-vuex-i18n` dependency using yarn or npm to your project
- Add `@pencilpix/nuxt-vuex-i18n` to `modules` section of `nuxt.config.js`

```js
{
  modules: [
    // Simple usage
    '@pencilpix/nuxt-vuex-i18n',

    // With options
    ['@pencilpix/nuxt-vuex-i18n', { /* module options */ }],
 ]
}
```

## Usage

Module Description

## Development

- Clone this repository
- Install dependnecies using `yarn install` or `npm install`
- Start development server using `npm run dev`

## License

[MIT License](./LICENSE)

Copyright (c) Mohamed hassan <pencilpix11@gmail.com>
