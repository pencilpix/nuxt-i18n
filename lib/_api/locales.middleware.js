const express = require('express')
const router = express.Router()
const { resolve } = require('path')
const FileManager = require('./FileManager')
const localeManager = new FileManager('./locales')
let languages = localeManager.get('languages.json')
let auth = localeManager.get('.auth.config.json')
let locales = {}

const langsPath = '/languages'
const localesPath = '/locale'
const createPath = '/create_locale'


var app = express()

const updateLanguages = () => {
  languages = localeManager.get('languages.json')
}

/**
 * check if value is object or not
 * @param {any} obj value to test
 * @return {Boolean}
 */
const isObject = (obj) => {
  const type = Object.prototype.toString.call(obj).replace(/(\[\w+\s)|(\]$)/g, '');
  return type.toLowerCase() == 'object';
}


/**
 * makes all express methods available
 * for request and response.
 */
router.use((req, res, next) => {
  Object.setPrototypeOf(req, app.request)
  Object.setPrototypeOf(res, app.response)
  req.res = res
  res.req = req
  next()
})

/**
 * get languages list from
 * /locales/languages.json
 */
router.get(langsPath, (req, res) => {
  languages.then(langs => {
    return res.status(200).json(langs)
  })
})

router.post(localesPath, (req, res) => {
  const { code } = req.body

  languages.then(langs => {
    const isLang = langs.find(lang => lang.code === code)

    if (!isLang) {
      return res.status(404).json({ status: 'error', error_message: `The language ${code} is not found`})
    }

    if (!locales[code]) {
      localeManager.get(`${code}.json`).then(locale => {
        locales[code] = locale;
        return res.status(200).json(locales[code])
      })
      return;
    }
    return res.status(200).json(locales[code])
  })
})



/**
 * languages: [{ code, iso, name, dir, is_default }]
 * translations: { [code]: { phrase: 'translation' }}
 * if authentication provide username and password as strings.
 */
router.post(createPath, (req, res) => {
  auth.then((credentails) => {
    const { languages, translations, username, password } = req.body
    const langList = typeof languages === 'string' ? JSON.parse(languages) : languages
    const transList = typeof translations === 'string' ? JSON.parse(translations) : translations
    const isLanguages = !!langList && Array.isArray(langList)
    const isTranslations = !!transList && isObject(transList)
    const required = { languages: isLanguages, translations: isTranslations }
    const isAuthRequired = credentails.username && credentails.password;
    const isAuthenticated = credentails.username === username && credentails.password === password;

    if (isAuthRequired && !isAuthenticated) {
      return res.status(401).json({
        status: 'error',
        error_message: 'You are not authorized'
      });
    }

    if (!isLanguages || !isTranslations) {
      return res.status(422).json({
        status: 'error',
        error_message: Object.keys(required)
          .filter(field => !required[field])
          .map(field => ({ [field]: 'The ' + field + ' is required'}))
      });
    }

    Promise.all([
      localeManager.set('languages.json', langList),
      Object.keys(transList).map(code => localeManager.set(`${code}.json`, transList[code]))
    ]).then(() => {
      updateLanguages();
      locales = {};
      res.status(200)
        .json({ status: 'success', message: '[nuxt-vuex-i18n]: locales successfully created'})
    })
  })
})


module.exports = router
