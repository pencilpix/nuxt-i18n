const fs = require('fs');
const path = require('path');

function FileManager(pathStr) {
  this.$root = process.cwd();
  this.$dir = path.resolve(this.$root, pathStr);
}


FileManager.prototype = {
  /**
   * set the current directory to some other directory
   * it starts from root as example: public/assets/
   * the projectRoot/public/assets will be the destination folder for
   * the file to set/get data to/from.
   * @param {String} pathStr new path to put/get files from.
   */
  set dir(pathStr) {
    this.$dir = path.resolve(this.$root, pathStr);
  },


  /**
   * get the directory to put/get files to/from.
   * @return {String}
   */
  get dir() {
    return this.$dir;
  },

  /**
   * read a file from the current dir
   * and return its json value parsed.
   * @param {String} file file name
   */
  readFile(file) {
    const data = fs.readFileSync(`${this.dir}/${file}`, { encoding: 'utf-8' });
    if (data) {
      return JSON.parse(data);
    }
    return {};
  },


  /**
   * return parsed JSON file content.
   * @param {String} file json file name as: 'file.json', 'dir/file.json', ...etc.
   * @return {Promise}
   */
  get(file) {
    let data = '';
    if (!this.directoryExist()) {
      console.log('[FileManager]: directory is not found: ', this.dir);
      this.createDirectory();
    }

    if (!this.fileExist(file)) {
      return this.set(file, {}).then(() => {
        return Promise.resolve(this.readFile(file));
      });
    }

    return Promise.resolve(this.readFile(file));
  },


  /**
   * convert data to a JSON and save it to some path.
   * @param {String} file filename as: 'file.json'
   * @param {Object|Array} data information to save at the file.
   */
  set(file, data) {
    const dataJSON = data ? JSON.stringify(data) : data;
    return new Promise((resolve, reject) => {
      fs.writeFile(`${this.dir}/${file}`, dataJSON, (err) => {
        if (err) {
          console.log(err);
          reject(`[FileManager]: error occured when set data to the ${file} file.`, err.stack);
          return;
        }
        console.log(`[FileManager]: data has been set to the ${file} file successfully`);
        resolve(true);
      });
    })
  },

  /**
   * override data in some file
   * @param {String} file filename as: 'file.json'
   * @param {Object|Array} data data to override other data in the file
   */
  override(file, data) {
    const newData = this.get(file);
    Object.keys(data).forEach((key) => {
      newData[key] = data[key];
    });

    this.set(`${this.dir}/${file}`, newData);
  },


  /**
   * checks if file is exist under the working directory or not
   * @param {String} file filename as: 'file.json'
   * @return {Boolean}
   */
  fileExist(file) {
    let isFile = false;

    try {
      isFile = fs.statSync(`${this.dir}/${file}`).isFile();
    } catch (e) {
      isFile = false;
    }

    return isFile;
  },

  directoryExist() {
    return fs.existsSync(this.dir);
  },

  createDirectory() {
    console.log('creating directory', this.dir);
    fs.mkdirSync(this.dir);
  },
};


module.exports = FileManager;
