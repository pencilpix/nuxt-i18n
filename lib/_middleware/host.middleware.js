import Vue from 'vue'
import middleware from './middleware'
import Cookies from 'js-cookie'
import Cookie from 'cookie'



const pluginOpts = <%= JSON.stringify(options) %>
const useDefaultAsRoot = pluginOpts.useDefaultAsRoot
const url = pluginOpts.apiUrl + pluginOpts.transEndpoint


const errorMsgs = {
  noTranslations: '[nuxt-vuex-i18n]: There is no Translations found for: ',
  noAppUrl: {
    statusCode: 500,
    message: '[nuxt-vuex-i18n]: appUrl option must be set to make hostLocale work'
  }
}


const isInIgnoredPaths = (path) => {
  const ignoredPaths = pluginOpts.ignorePaths || []
  let isIgnored = false

  ignoredPaths.forEach(_path => {
    const pattern = new RegExp('^' + _path)
    if (pattern.test(path)) {
      isIgnored = true
    }
  })
  return isIgnored
}


const validLang = (langs, path) => {
  const pattern = /^\/\w\w\/?$/
  const param = path.slice(0, 4)
  const code = path.slice(1, 3)
  const isValid = pattern.test(param)
  const isAvail = !!langs[code]
  return {
    valid: isValid,
    available: isAvail,
    code,
  }
}



/**
 * fetch locale from network and add it to the store.
 * @param {Object} app Nuxt app instance
 * @param {Store} store app state
 * @param {String} code language code
 */
function fetchLocale(app, store, code) {
  const storeLocale = store.getters['$vuexI18n/localeFromLang'](code)
  if (storeLocale) {
    return Promise.resolve({ code, messages: storeLocale })
  }

  return app.$axios.$post(url, { code })
    .then((trans) => {
      const translation = { code, messages: trans }
      store.dispatch('$vuexI18n/loadLocale', translation)
      return translation
    }).catch(err => {
      console.warn('[nuxt-vuex-i18n]: There is no Translations found for: ', code)
      return null
    })
}





middleware['vuex-i18n'] = async function (ctx) {
  const { app, store, route, redirect, error, req, isHMR } = ctx
  const langs = store.getters['$vuexI18n/langs']
  const defaultLang = store.getters['$vuexI18n/defaultLang']
  const cookiesStr = process.client ? document.cookie : req ? req.headers.cookie : ''
  const cookies = Cookie.parse(cookiesStr || '') || {}
  const userLang = cookies['vuexI18n_user_lang']
  const validated = validLang(langs, route.path)
  const pathLang = route.path.slice(1, 3)
  let current
  let fallback

  if (isInIgnoredPaths(route.path) || isHMR) {
    return
  }

  if (route.path === '/') {
    return redirect({
      name: 'index',
      params: { lang: userLang || defaultLang.code || 'en' },
      query: route.query,
    })
  } else if (!validated.valid && !validated.available) {
    return redirect('/' + (userLang || defaultLang.code) + route.fullPath)
  } else if (validated.valid && !validated.available) {
    return redirect(route.fullPath.replace(validated.code, defaultLang.code))
  } else {
    current = await fetchLocale(app, store, pathLang)
    store.dispatch('$vuexI18n/useLocale', current && current.code)
    if (pathLang !== defaultLang.code) {
      fallback = await fetchLocale(app, store, defaultLang.code)
      // store.dispatch('$vuexI18n/useLocale', fallback && fallback.code)
    }
  }

  if(process.client && current && current.code !== userLang) {
    Cookies.set('vuexI18n_user_lang', pathLang, {
      expires: 365
    })
  }
}

