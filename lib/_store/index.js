import Vue from 'vue'


const replaceVars = (phrase, vars = {}) => {
  let newPhrase = phrase
  const isCurly = /\{\s?[a-z0-9_]+\s?\}/.test(phrase)
  const isColon = /\:\:\s?[a-z0-9_]+\s?\:\:/.test(phrase)
  const leftSep = isCurly ? '\\{' : isColon ? '\\:\\:' : ''
  const rightSep = isCurly ? '\\}' : isColon ? '\\:\\:' : ''


  Object.keys(vars).forEach(key => {
      const pattern = new RegExp(leftSep + '\\s?' + key + '\\s?' + rightSep, 'gi')
      newPhrase = newPhrase.replace(pattern, vars[key])
  })
  return newPhrase
}



const createState = () => ({
  defaultLang: '',
  fallbackLang: '',
  lang: '',
  langs: {},
  locales: {},
})



const vuexI18nStore = () =>({
  namespaced: true,

  state: {
    defaultLang: '',
    fallbackLang: '',
    lang: '',
    langs: {},
    locales: {},
    notFound: {
      /*
      [key]: { en: true }
      */
    },
  },



  mutations: {
    LOAD_LANGUAGES(state, languages) {
      languages.forEach((lang) => {
        Vue.set(state.langs, lang.code, lang)
        if (lang.is_default) {
          Vue.set(state, 'defaultLang', lang.code)
        }
      })
    },

    SET_FALLBACK(state, code) {
      Vue.set(state, 'fallbackLang', code)
    },

    SET_DEFAULT(state, code) {
      Vue.set(state, 'defaultLang', code)
    },

    LOAD_LOCALE(state, locale) {
      Vue.set(state.locales, locale.code, locale.messages)
    },

    USE_LOCALE(state, code) {
      Vue.set(state, 'lang', code)
    },

    ADD_NOT_FOUND(state, { phrase, result }) {
      Vue.set(state.notFound, phrase, result)
    },
  },



  actions: {
    loadLanguages({ commit }, langList) {
      commit('LOAD_LANGUAGES', langList)
    },

    loadLocale({ commit }, locale) {
      commit('LOAD_LOCALE', locale)
    },

    useLocale({ commit }, code) {
      commit('USE_LOCALE', code)
    },

    setFallback({ commit }, code) {
      commit('SET_FALLBACK', code)
    },

    setDefault({ commit }, code) {
      commit('SET_DEFAULT', code)
    },
  },



  getters: {
    // return list of all locales available
    locales: state => state.locales,
    // return current locale.
    locale: state => state.locales[state.lang],
    // get locale of a language
    localeFromLang: state => code => state.locales[code],
    // list of available languages
    langs: state => state.langs,
    // get current language
    lang: state => state.langs[state.lang],
    // get default language
    defaultLang: state => state.langs[state.defaultLang],
    // get fallback language
    fallbackLang: state => state.langs[state.fallbackLang],
    // message from current locale
    message: (state, { locale }) => phrase => {
      const msg = locale && locale[phrase]
      return { msg, current: !msg };
    },
    // message from fallback locale
    fallbackMessage: (state, { localeFromLang }) => phrase => {
      const locale = localeFromLang(state.fallbackLang)
      const msg = locale && locale[phrase]
      return { msg, fallback: !msg };
    },


    // message from default locale
    defaultMessage: (state, { localeFromLang }) => phrase => {
      const locale = localeFromLang(state.defaultLang)
      const msg = locale && locale[phrase]
      return { msg, default: !msg };
    },


    // translate a message.
    translate: (state, getters) => (phrase, vars) => {
      const { message, fallbackMessage, defaultMessage } = getters
      let trans = message(phrase)

      if (!trans.msg && state.fallbackLang) {
        trans = Object.assign({}, trans, fallbackMessage(phrase))
      }

      if (!trans.msg && state.defaultLang) {
        trans = Object.assign({}, trans, defaultMessage(phrase))
      }

      if (trans.msg && typeof vars !== 'undefined') {
        trans = { ...trans, msg: replaceVars(trans.msg, vars) }
      }

      if (trans && trans.msg) return trans
      return Object.assign({}, trans, { msg: phrase })
    },
  },
});

export default function ({ store }) {
  const opts = {};

  if (process.client) {
    opts.preserveState = true;
  }
  store.registerModule('$vuexI18n', vuexI18nStore(), opts);
}

