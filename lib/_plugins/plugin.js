import Vue from 'vue'


const pluginOpts = <%= JSON.stringify(options) %>
const langsUrl = pluginOpts.apiUrl + pluginOpts.langsEndpoint
const url = pluginOpts.hostLocales ? langsUrl : pluginOpts.appUrl + langsUrl



/**
 * return list of available languages and save it to the store if
 * not already found in the store.
 * @param {Object} app Nuxt app instance
 * @param {Store} store app state
 * @param {Function} error callback of nuxt error
 */
function fetchLanguages(app, store, error) {
  const storeLangs = store.getters['$vuexI18n/langs'] || {}
  const storeLangsList = Object.keys(storeLangs).map(key => storeLangs[key])

  if (storeLangsList.length > 0) {
    return Promise.resolve(storeLangsList)
  }

  if (!url) {
    error({
      message: '[nuxt-vuex-i18n]: appUrl option must be set to make hostLocale work',
      code: 500
    })
  }

  return app.$axios.$get(url)
  .then(languages => {
    store.dispatch('$vuexI18n/loadLanguages', languages)
    return languages
  }).catch(err => {
    console.warn('[nuxt-vuex-i18n]: could not get the available languages from ' + url, err)
    error({ statusCode: 404, message: 'Page Not found'})
  })
}


export default async function ({ app, store, error, route }, inject) {
  const languages = await fetchLanguages(app, store, error)

  if (!languages) {
    console.warn('[vuexI18n]: languages is not loaded: ', languages)
  }


}
