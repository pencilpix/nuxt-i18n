import Vue from 'vue'

/**
 * vue plugin to manage the locales
 * in the store module '$vuexI18n'
 */
const vuexI18n = {
  $store: {},

  notFound: {},

  /**
   * set the store instance to manage locale state
   * @param{Object} storeInstance Store instance that manage vue app state.
   */
  set store(storeInstance) {
    this.$store = storeInstance;
  },


  get notFoundList() {
    return Object.values(this.notFound)
  },

  get notFoundKeysJson() {
    const list = Object.keys(this.notFound)
      .reduce((cur, next) => ({ ...cur, [next]: "" }), {})

    return JSON.stringify(list, null, '  ')
  },


  /**
   * return store
   */
  get store() {
    return this.$store;
  },


  /**
   * get current language
   * @return {Object}
   */
  get lang() {
    return this.store.getters['$vuexI18n/lang'] || {};
  },


  /**
   * current direction
   * @return {String}
   */
  get dir() {
    return this.lang.dir;
  },


  /**
   * get current language code
   * @return {String}
   */
  get code() {
    return this.lang.code;
  },


  /**
   * get current language code
   * @return {String}
   */
  get iso() {
    return this.lang.iso;
  },



  /**
   * languages object available in the store
   * @return {Object} {code: { code, name, native, dir }}
   */
  get langs() {
    return this.store.getters['$vuexI18n/langs'];
  },


  // get validatorLocale() {
  //   return this.store.getters.getValidatorLocale;
  // },


  /**
   * fetch translation of a phrase from the state.
   * @param {String} phrase to translate.
   * @return {String}
   */
  $t(phrase, vars) {
    const result = this.store.getters['$vuexI18n/translate'](phrase, vars)
    const { msg } = result
    const isNotFound = this.notFound[phrase]

    if (msg === phrase) {
      this.$onNotFound(phrase, vars)
    }

    if (msg === phrase && !isNotFound) {
      if (process.env.dev) {
        console.error(
          '[vuexI18n] new phrase needs translation: ',
          JSON.stringify({ ...result, vars }),
        )
      }
      this.notFound[phrase] = { ...result, vars }
    }

    return msg;
  },


  /**
   * if translation is not found this function
   * will be called.
   * override it to do some action on phrase and result
   * like post request on the original api to create this phrase for example.
   * @abstract
   * @param {String} phrase
   * @param {Object} vars
   */
  $onNotFound(phrase, vars) {
  },


  trans(phrase, vars) {
    const msg = 'trans method is deprecated and will be removed in next release'
    console.warn('[vuexI18n]: ' + msg + ' please use $t')
    return this.$t(phrase, vars)
  },


  /**
   * set the current language code.
   * @param {String} code language code
   */
  set(code) {
    return this.store.dispatch('$vuexI18n/useLocale', code);
  },
};


const LocaleVuePlugin = {
  install($Vue) {
    $Vue.prototype.$vuexI18n = vuexI18n
    $Vue.prototype.$t = (phrase, vars) => vuexI18n.$t(phrase, vars)
  }
}


Vue.use(LocaleVuePlugin)


export default function localePlugin(ctx, inject) {
  if (!ctx.app.$vuexI18n) {
    inject('vuexI18n', vuexI18n)
  }

  ctx.app.$vuexI18n.store = ctx.store
}
