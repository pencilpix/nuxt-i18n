module.exports = function handleRoutes(routes, useDefaultAsRoot) {
  const newRoutes = []
  routes.forEach(route => {
    // route.path = '/:lang' + route.path
    newRoutes.push({
      ...route,
      path: '/:lang' + route.path,
    })
  })

  return newRoutes
}
