const { resolve } = require('path')
const handleRoutes = require('./_utils/handle-routes')
const routerMiddleware = require('./_api/locales.middleware')
const FileManager = require('./_api/FileManager')
const localeManager = new FileManager('./locales')


const DEFAULTS = {
  loadFromExternalApi: false,
  // external or internal app url
  appUrl: 'http://localhost:4000',

  // external api end points
  apiUrl: '/vuexi18n/locales', // external/internal api
  langsEndpoint: '/languages', // external/internal api available languages list.
  transEndpoint: '/locale', // external/internal api locale endpoint post request will recieve code.
  hostLocales: true,

  asyncLoadFromCallback: false, // if true will use asyncLoadTranslation to get the { languages, translations }
  asyncLoadTranslations: null, // callback that if it's there will disable

  fallback: 'en',
  default: 'ar',
  useDefaultAsRoot: false,
  enableSeo: false,

  auth: {
    username: null,
    password: null,
  },

  ignorePaths: ['/static'],
}


module.exports = async function module (moduleOptions) {
  const options = Object.assign({}, DEFAULTS, moduleOptions, this.options.i18n)
  const auth = options.auth.username ? options.auth : {}
  const authFile = await localeManager.set('.auth.config.json', auth)


  if (options.hostLocales) {
    // if hot locales disable end points
    options.langsEndpoint = '/languages'
    options.transEndpoint = '/locale'
    this.addServerMiddleware(require('body-parser').urlencoded({ extended: true }))
    this.addServerMiddleware(require('body-parser').json())
    this.addServerMiddleware({ path: options.apiUrl, handler: routerMiddleware})
  }

  this.nuxt.hook('build:before', (ctx) => {
    ctx.nuxt.moduleContainer.addPlugin({
      src: resolve(__dirname, './_store/index.js'),
      fileName: 'vuex-i18n.store.js'
    })
  })


  this.addPlugin({
    src: resolve(__dirname, './_plugins/vuex-i18n.js'),
    fileName: 'vuex-i18n.plugin.helper.js',
    options
  })

  // extending routes
  this.extendRoutes((routes) => {
    const newRoutes = handleRoutes(routes, options.useDefaultAsRoot)
    routes.splice(0, routes.length)
    routes.unshift(...newRoutes)
  })




  this.addPlugin({
    src: resolve(__dirname, './_plugins/plugin.js'),
    fileName: 'vuex-i18n.plugin.js',
    options
  })


  // routing middlewares
  if (options.hostLocales) {
    this.addPlugin({
      src: resolve(__dirname, './_middleware/host.middleware.js'),
      fileName: 'vuex-i18n.routing.middleware.js',
      options
    });
  } else if (options.loadFromExternalApi) {
    this.addPlugin({
      src: resolve(__dirname, './_middleware/external.middleware.js'),
      fileName: 'vuex-i18n.routing.middleware.js',
      options
    });

  } else {
    this.addPlugin({
      src: resolve(__dirname, './_middleware/callback.middleware.js'),
      fileName: 'vuex-i18n.routing.middleware.js',
      options
    });
  }

  this.options.router.middleware.push('vuex-i18n')

  if (this.options.build.vendor) {
    this.options.build.vendor.push('body-parser', '@nuxtjs/axios')
  }

  // add axios-module register
  this.requireModule(['@nuxtjs/axios'])
}

module.exports.meta = require('../package.json')
